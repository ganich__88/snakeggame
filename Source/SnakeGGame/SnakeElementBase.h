// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElementBase.generated.h"

//�������� ����������� ����� ������ �� ��� ����������, � � ����� *.cpp ����� ��� �����������
class UStaticMeshComponent;
class ASnakeBase;
class APlayerPawnBase;

UCLASS()
class SNAKEGGAME_API ASnakeElementBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	//��������� �� UStaticMeshComponent, ���������� ���������� � ������������ �����,
	// �.�. � *.h �� ������������� ���������� ����� ����� ������� ��� �����(class UStaticMeshComponent)
	UPROPERTY(VisibleAnywhere, BluePrintReadOnly)
	UStaticMeshComponent* MeshComponent;

	// ��������� �� ������ ������� ��������� ������ ������� OnComponentBeginOverlap
	UPROPERTY()
	ASnakeBase* SnakeOwner;

	UPROPERTY(BlueprintReadWrite)
	APlayerPawnBase* PawnBase;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// ����� ������� ������ ��� ������� ������ ����� ������
	// ������ ������ ������������� ������ ������ � �������� � � AddSnakeElement
	// ����������������� � �������� �� �++ �� �������������,
	// ��� ������ ����� ���������� �������� BlueptintNativeEvent
	// �� ����������� ����� � ������������� ����������
	
	UFUNCTION(BlueprintNativeEvent)
	void SetFirstElementType();
	// ��� �� ���������� ��� ��� ������� ���� �� �����
	// ���������� �������� �� �������� � ������� ������, � ���������� ����������� ������

	void SetFirstElementType_Implementation();
	// ������ ����� � Snake ElementBP ������� ���� SetFirstElementType �� ������� ������� Call to Parent functionn
	// � ������� �������� ������� ��������, �.�. ������



	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	// ����� ������� �������� �������, � ������������� ����������� ����������
	// �������� ���������, ��������� � ����� ������� ������������
	// ������ �������� ��������� ������� ������ ������� ������ ������� �������
	// int32 ��������� ��� ���� ��� �� ��������� ��� ����� �� ������� ��������
	// ����� ��� ���� ��� �� ������ ������ �������, � �� �������
	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComponent,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

	UFUNCTION()
	void ToggleCollision();

};
