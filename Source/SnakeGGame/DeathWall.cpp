// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/Classes/Components/BoxComponent.h"
#include "DeathWall.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"


// Sets default values
ADeathWall::ADeathWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	
	class UStaticMesh* WallMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;

	//��������� �������� MaterialInstanceConstant'/Game/DeathMaterial_Inst.DeathMaterial_Inst'
	WallColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/DeathMaterial_Inst.DeathMaterial_Inst'")).Get();

	//������ �������� ������� ��� ������
	MyRootComponent = CreateDefaultSubobject<UBoxComponent>("RootModel");

	RootComponent = MyRootComponent;

	class UStaticMeshComponent* DeathWall;

	DeathWall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WALL"));

	DeathWall->SetStaticMesh(WallMesh);

	DeathWall->SetRelativeLocation(FVector(0, 0, 0));

	DeathWall->SetMaterial(0, WallColor);
	
	DeathWall->AttachTo(MyRootComponent);
	
}

// Called when the game starts or when spawned
void ADeathWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADeathWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ADeathWall::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		for (int i = 0; i < Snake->SnakeElements.Num(); i++)
		{
			Snake->SnakeElements[i]->Destroy();
		}

		APlayerPawnBase* pawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (pawn)
		{
			pawn->SnakeDestroy();

		}

	}
}