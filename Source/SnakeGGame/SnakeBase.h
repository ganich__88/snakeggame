// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

//�������� ����������� ����� ������ �� ��� ����������, � � ����� *.cpp ����� ��� �����������
class ASnakeElementBase;
class APlayerPawnBase;

//���������� enum ��� ������������ ���������� �������� ������
UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGTH
};


UCLASS()
class SNAKEGGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	bool InputMove;

	//��������� �� ��� �����
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	//���������� ��� �������� ��������� ��������� ������ ���� �� ������
	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	//�������� ���������� �� �������� �������� ������
	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	// ������ ��� ������ � �++ ������� � () ������ �� �����
	// ����� ����� ���������������� ���� ������ �������� � �������� � �������( �� ���������� �� �������� )
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	//��� ���������� ���������� ����������� �������� 
	UPROPERTY()
	EMovementDirection LastMoveDirection;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<APlayerPawnBase> PawnClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// ����� ���������� ����� ���������
	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);

	
	//����� �������� ����������� �� ���� DeltaTime
	//� � ����� ������ ����� ��������� ��������� ����������� �������� � ( �������� DeltaTime) ������������ � ������ ��������!!!!!
	UFUNCTION(BlueprintCallable)
	void Move();

	// ����� ������� �������� � ��� ��� ��� ���� ���������� � ��� ����
	// � ( �������, ����� ������ ���� ������ ������ �������, Other ������� Actora � ������� ���������� ��� ���� ) 
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	int32 score = 0;

};
