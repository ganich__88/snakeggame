// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeGGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeGGame, "SnakeGGame" );
