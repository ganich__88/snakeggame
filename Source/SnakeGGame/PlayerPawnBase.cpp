// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Components/InputComponent.h"


// Sets default values ����������� PAWN, �� ������ ������� ��� � GameMode
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	// ���������� � ���������� �������� ��������� <��������� ����������� �����>(��� ������ ����������)
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	// ����� ����� �������� VTransform FRotator ��� ����� ������ 
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	//������������ ������ ����
	SetActorRotation(FRotator(-90, 0, 0));
	
	//��� ������ ������ Actor
	
	//CreateSnakeActor();
	
	CreateFoodActor();
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// � ���������� Unreal ������� ������ ��������
	// �������� � PlayerInputComponent ��� Axis ������� � ����������
	// this ��� ��������� �� ����� � ������� ���������� �����
	// � ����� ����� & ��������� b�� ������ Vertical � Horizontal ������
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	
}

//��� ������ ���������� ���������� SnakeBase.h
void APlayerPawnBase::CreateSnakeActor()
{
	//GetWorld - ��� ������ SpawnActor
	//� ������� ������� ��� ����� ��� ���(� ASnakeBase::StaticClass() � � ��������� ��������� FTransform())
	// �.�. �� ������� ���������� TSubclassOf<ASnakeBase> SnakeActorClass; �� ����� ����� � ������������
	if (GetWorld())
	{
		SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());

		GameMode = 1;

	}
}


void APlayerPawnBase::CreateFoodActor()
{
	if (GetWorld())
	{
		FVector NewLocation(FMath::FRandRange(-1370, 370), FMath::FRandRange(-1370, 1370), 0);

		FTransform NewTransform(NewLocation);

		FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, NewTransform);
				
	}
}


void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (GameMode > 0)
	{
		//���� ��������� SnakeActor ������� �� � ����������� value, ������� ���� 1, ���� -1, �� ������ ����������� �������� ������
		if (IsValid(SnakeActor) && SnakeActor->InputMove)
		{
			//���� �������� > 0, �� ������ ������� �����
			// �������� ��������, ��� �� �� �������� ���� � ���� �� Y
			// ���� �������� �� ����� ����, �� ������������� �����
			if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
			{
				//��������� ����������� ���������� LastMoveDirection
				// � ��������� � enum class ��������� ��� ��������
				SnakeActor->LastMoveDirection = EMovementDirection::UP;
				SnakeActor->InputMove = false;
			}
			//���� �������� < 0, �� ������ ������� ����
			else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
			{
				SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
				SnakeActor->InputMove = false;
			}
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (GameMode > 0)
	{
	//���� ��������� SnakeActor ������� �� � ����������� value, ������� ���� 1, ���� -1, �� ������ ����������� �������� ������
		if (IsValid(SnakeActor) && SnakeActor->InputMove)
		{
			//���� �������� > 0, �� ������ ������� ������
			// �������� ��������, ��� �� �� �������� ���� � ���� �� �
			if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
			{
				//��������� ����������� ���������� LastMoveDirection
				// � ��������� � enum class ��������� ��� ��������
				SnakeActor->LastMoveDirection = EMovementDirection::RIGTH;
				SnakeActor->InputMove = false;
			}
			//���� �������� < 0, �� ������ ������� �����
			else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGTH)
			{
				SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
				SnakeActor->InputMove = false;
			}
		}
	}
}


int32 APlayerPawnBase::GetScore()
{
	if(SnakeActor)
	{ 
		return SnakeActor->score;
	}
	return 0;
}

void APlayerPawnBase::SnakeDestroy()
{
	GameMode = 0;
	if (SnakeActor) SnakeActor->Destroy(true, true);
}
