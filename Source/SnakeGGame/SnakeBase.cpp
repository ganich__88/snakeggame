// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

#include "PlayerPawnBase.h"

// ���������
#include "Interactable.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	//�������� �� ��������� ElementSize, �� ������ �������� �������������� � ����� 100
	//������������ � SnakeBP
	ElementSize = 60.f;
		
	// �������� �� ���������
	MovementSpeed = 0.5f;
	
	// �� ��������� �������� ����� ����
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	



	// ��� ����� ������� ����
	// ������ MovementSpeed �������� �������, � ������ TICK
	SetActorTickInterval(MovementSpeed);
	// ����� � SnakeBP ������� ��� 0,5 ��� ������� 1 ������� � ��
	
		

	//�������� SnakeElementBase ��� ��������, �������� ������ � AddSnakeElement()
	//GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform());
	
	//�������� ����� AddSnakeElement � � ( ��������� ���-�� ��������� �� ������ ����)
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//DeltaTime ���������� ��� �� ���������� �� ����������� �� ������

	InputMove = true;
	
	Move();
	
	score++;

}

void ASnakeBase::AddSnakeElement(int ElementNum)
{
	
	//���������� ���� ��� ���-�� ��������� ������� ���������� ����������
	//���� ����� 1 ������� �� ������ ���� � �� ������������ � ���������� � ������
	for (int i = 0; i < ElementNum; i++)
	{
		
		/* � ������ ������� ����� ���-�� ���������
		 ���� num ����� 0 �� �� ������ �� �������� ���� ������ 0 �� �������� �� ElementSize
		SnakeElements.Num()* ElementSize; ��������� ��� �������� ������ GetActorTransform(),
		� ������ (SnakeElementClass, FTransform(GetActorLocation()-FVector(SnakeElements * ElementSize, 0, 0)) */
		
		/* ��� �������� ������ � ������ �� GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform()); ������
		   ��������� ��� � ���������� auto NewSnakeElem =
		   ��� �� �� �� GetActorTransform(), ��� �������� ����� � ����� �����, ������� �������� ���������� ElementSize*/
		 //auto NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, FTransform(GetActorLocation()-FVector(SnakeElements.Num()*ElementSize, 0, 0)));
		 
		
		 //��� ��������� ���� �������� ��� �� ����������
		 //FVector ���������� ��� ���������� ���������� �
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		
		//���������� NewLocation ��� ������������� ������ Transform
		FTransform NewTransform(NewLocation);
		
		//�.�. ������ ������������ NewLocation � � NewTransform ���������� ���� �������, ��������� ��� � ���������� � �������� � ������
		// ASnakeElementBase* ���� ��������� ���
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		
		NewSnakeElem->SetActorHiddenInGame(true);

		// ��� ���������� ������ ����� ������������� ������� ������� SnakeOwner � ������������� ��� � this
		NewSnakeElem->SnakeOwner = (this);
				
		// � ������������ Actor, � �������� ������� ������������� ��������� KeepRelativeTransform
		// �.�. AttachToActor ������ ������ �������� ������ �� ���������, �� �������� �� ���� ������ � �������� ���� � Move
		//NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		// 
	 
		//���������� � ������� SnakeElements � ����� �������� Add � (������� ������� ���������� ��������)
		// int32 ElemIndex - �������� ������� ������
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		// ��������� �������� ������ ������
		// ���� ����� ������� �������� ������, �� ���������� �� ���� ��������� �������
		// SnakeElements.Find(NewSnakeElem); �.�. Add � ��� ���������� ������ ������� ������� ��� � ����������

		// �������� ������ ��������, ���� �� ����� 0 �� �� ����� ������ � �������
		// ������ ��� ������ ������
		if (ElemIndex == 0)
		{
			// ������ � SnakeElementBase ����� SetFirstElementType() ������� ������� ��� �� ����� ������ 
			// � �������� ��� 
			NewSnakeElem->SetFirstElementType();

		}

	}
		
}


void ASnakeBase::Move()
{
	// ���� ������� ������ � ����������� �� ��������� ENUM
	// ������ �� ���� �.�. ����� 0
	FVector MovementVector(0, 0, 0);

	/* ��� �������� ������������!!!!!������������ � ������ �������� !!!!!
	float MovementSpeedDelta = MovementSpeed * DeltaTime;
	*/

	// �������� ��� Vector ����������������, ����� �������� ���������� MovementSpeed � ElementSize
	MovementSpeed = ElementSize;

	/* ������� ����������� �� if �� switch
	*
	*	//���� LastMoveDirection ����� �����, �� MovementVector ����� FVector � ������������� �
	*	if (LastMoveDirection == EMovementDirection::UP)
	*	{
	*		// ��� �� ���������� �� ����������� ������ ������ MovementVector ������ ������������ MovementSpeed * DeltaTime
	*		//���� MovementVector = FVector(MovementVector, 0, 0); �������� ��������� MovementSpeedDelta = MovementSpeed * DeltaTime
	*		//�.�. �� ����� � ����� ������������, ������� MovementVector = FVector(MovementSpeedDelta, 0, 0);
	*		MovementVector = FVector(MovementSpeedDelta, 0, 0);
	*	}
	*/
	
	
		//��������� �� LastMoveDirection
		//��� �������� ������������  MovementVector.X += MovementSpeedDelta; � ��.
		switch (LastMoveDirection)
		{
			//���� ������ �������� �����, �� MovementVector �� � += movementSpeedDelta
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			break;
			//���� ������ �������� ����, �� MovementVector �� � -= movementSpeedDelta
		case  EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
			//���� ������ �������� �����, �� MovementVector �� Y += movementSpeedDelta
		case EMovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			break;
			//���� ������ �������� ������, �� MovementVector �� Y -= movementSpeedDelta
		case EMovementDirection::RIGTH:
			MovementVector.Y -= ElementSize;
			break;
		}


		// ��� ����������� Actor, � AddSnakeElement ������ ��� ������ ������������� Actor
		// AddActorWorldOffset(MovementVector);

		// ���� ����������� �� ������
		// � ����� ����� ����������� ������, ������� ����������� � ������ ��� ����� �� �������
		// ��� � ����� ������ �������, ������ ���������� �������� SnakeElements.Num()
		// �.�. � �++ � Unreal ���������� ���������� � 0, ���������� ������� �������


		// �������� ����� ����� ��� ��� ������� ��� �����
		// ������� �������� 0 ������� � �������� �� ��� �����
		SnakeElements[0]->ToggleCollision();
			
		SnakeElements[0]->SetActorHiddenInGame(false);
		

		for (int i = SnakeElements.Num() - 1; i > 0; i--)
		{
			
			// �������� ������� �������
			auto CurrentElement = SnakeElements[i];

			// ���������� �������
			auto PrevElement = SnakeElements[i - 1];

			// ������� ��� ������� � ����������� �����������
			// �������� � ����������� �������� ����� � ������������, ������� ����� ��� Location
			FVector PrevLocation = PrevElement->GetActorLocation();

			// � ������� ������� ������� �� ����� �����������
			CurrentElement->SetActorLocation(PrevLocation);

			SnakeElements[i]->SetActorHiddenInGame(false);
		}

		// ���������� ������ ������ �������� �� �����
		// ��� ���������� �� �������� �������
		// � AddActorWorldOffset ������� MovementVector
		SnakeElements[0]->AddActorWorldOffset(MovementVector);

		//�������� �������� ToggleCollision, ������� ���������, ����� ����������� ��� �����, ����������� ������ � �������� �������� �������
		SnakeElements[0]->ToggleCollision();
		
}



// ���������� ������ ������� ������ ������������ ��� ������������ ������� � ��� ����
void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		// ���� ��������� ������� �� ������� ����������� ����
		// ���������� � ����������, �� ��� ��� ���� �������� �������
		// ���� ���� ��� ������ ������ � ���� ��������� �������, �.�. ������
		// � ( ����� ������� OverlappedElement, � ���������� � ������� �� ����� ���������� ��� ������
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		
		//���� �����c ����� 0 �� ������� ������, �.�. ��� ������
		bool bIsFirst = ElemIndex == 0;
		// ������ ��������� �� ��� ����������, � ������� ��� ����� � ����� ���������� 
		//CAST<IINTERACTABLE>(OTHER);
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		
		// ��� ����������� �����?
		// ����� �������� �� ����������
		// ��������� ��������� �� �������
		if (InteractableInterface)
		{
			// �.�.��� �++ ������ � ��������� �������
			// �������� Interact � ( ������� ������, this) 
			// ������ this �������� , ��� �� ��� ������ ������� �.�. ������
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}




