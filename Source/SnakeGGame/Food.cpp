// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h" 
#include "PlayerPawnBase.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->SpawnActor<AFood>(FoodActorClass, GetActorTransform());
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}


void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		// �� ���������� �������� 1 �������
		// ������� �������� � ������
		auto Snake = Cast<ASnakeBase>(Interactor);
				
		if (IsValid(Snake))
		{
			
			Snake->AddSnakeElement();
			
			Snake->score++;
			
			Snake->SetActorTickInterval(Snake->GetActorTickInterval()*1.01);

			APlayerPawnBase* pawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
			if (pawn)
			{
				pawn->CreateFoodActor();

			}


			Destroy();

		}
	}
}


