// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

class ASnakeBase;
class APlayerPawnBase;


UCLASS()
class SNAKEGGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(BlueprintReadWrite)
	APlayerPawnBase* PawnBase;

	UPROPERTY(VisibleAnywhere, BluePrintReadOnly)
	UStaticMeshComponent* MeshComponent;

	//��������� �� ���
	UPROPERTY(BlueprintReadWrite)
	AFood* FoodActor;

	//��������� �� ����� ���
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodActorClass;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//������ ����� ������������� �.�. �� �� ����� Interactable
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
