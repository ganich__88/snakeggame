// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Interactable.h"
#include "PlayerPawnBase.generated.h"

//�������� ����������� ����� ������ �� ��� ����������, � � ����� *.cpp ����� ��� �����������
class UCameraComponent;
class ASnakeBase;
class AFood;

UCLASS()
class SNAKEGGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()


public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	//��������� �� ��������� ������, ���������� ���������� � ������������ �����,
	// �.�. � *.h �� ������������� ���������� ����� ����� ������� ��� �����(class UCameraComponent)
	// � SnakeGameMode ������ BP ������ PlayerPawnBP
	UPROPERTY(BluePrintReadWrite)
	UCameraComponent* PawnCamera;

	//��������� �� �������� ������ SnakeBase, ��� �� ���������� ���������� class ASnakeBase
	//��� �� ������ ������ Blueprint, ��� �� ���������� �� SnakeBase ������� BluePrint � ������� ��� � GameMode
	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;
	
	//��������� �� ��� ����� ������
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	//��������� �� ���
	UPROPERTY(BlueprintReadWrite)
	AFood* FoodActor;

	//��������� �� ����� ���
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodActorClass;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//������ ������ �� ������ ������ ������
	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	void CreateSnakeActor();


	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	void CreateFoodActor();

	// ������ �� ������� AXIS ��� ����������, �.�. �������� ���������� ���� float,
	// �� � �������� ������ ���� ���� float
	// ������ 2 ������ �� vertical � horizontal
	// !!!!!! �������� ��� ����������� ���� ���� ������� ���� 1 �����, ���� ������� ������� � ������
	// �� ������ ���������� � *.cpp ������������ ���� InputComponent.h
	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);
	
	// �������� �� �������� ���
	float StepDelay = 2.f;

	// ���������� �������
	float BufferTime = 0;

	//���������� ������ ����
	int32 GameMode = 0;

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	int32 GetGameMode() const { return GameMode; }

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
	int32 GetScore();

	void SnakeDestroy();

};
