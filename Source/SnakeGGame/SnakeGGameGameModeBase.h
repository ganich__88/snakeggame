// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGGAME_API ASnakeGGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
