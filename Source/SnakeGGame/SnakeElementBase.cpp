// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//����� UStaticMeshComponent
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	//������ ������������ ������ �� SnakeElementBase � � SnakeBP �������� ��� SnakeElemBP
	//��������������� � ����� SnakeElemBP ������� �� MeshComponent(Components) � Details ����� Static Mesh
	//RootComponent = MeshComponent;

	// ������ Mesh ������ ��� ��������� ���� � ��� ����������� ���� ������ ( �������� )
	// ����� �������� ��� Mesh ������� �� Overlap
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	// ����� �� ���� ������� � ������ ������������
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();

	
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ASnakeElementBase::SetFirstElementType_Implementation()
{
	//���� ���������� ������ �����, �� ��� ������ ������ � ������� �������� �� ���������
	
	// ��� ��������� � �������� ������ �������
	// � (��������� ����� � ������� ���������� ����� ������� ����� �������, ����� 
	//&��������� �� ��� ����� ����� �� ����������� ����� ��� ���������� ����� �������)
	// �� NewSnakeElem �������� MeshComponent, � ����� ������ ����������� �������
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);

}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)
{


	// ���������� ��������� ��� ������������ � ����� �������
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{	
		for (int i = 0; i < Snake->SnakeElements.Num(); i++)
		{
			Snake->SnakeElements[i]->Destroy();
		}

		APlayerPawnBase* pawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		if (pawn)
		{
			pawn->SnakeDestroy();
						
		}
		
	}

}

// ���������� ����� ���� ������ ������������ � ����������� OnComponentBeginOverlap
void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	// �������� ��� ����� ������ �������� ������
	// ��������� ���������� ��������� �� ������
	if (IsValid(SnakeOwner))
	{
		// ���� ��������� �������� �� �������� �����
		// ������� ����� �� SnakeBase.h SnakeElementOverlap()
		// � ( ������� ��� ���� �������, ������� ������ ����� ������������,
		// ������ �������� OtherActor ������� � ��� ���� ������)
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementBase::ToggleCollision()
{
	// ���� ����� Nocollision, �� QueryOnly
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	// � ��������� ������ ��������� ���� ��������
	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

