// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "DeathWall.generated.h"


class ASnakeBase;
class ASnakeElementBase;
class APlayerPawnBase;

UCLASS()
class SNAKEGGAME_API ADeathWall : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADeathWall();

	UPROPERTY(BlueprintReadWrite)
	APlayerPawnBase* PawnBase;
	
	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// ���� ������� ����� ������ ���� �����
	UPROPERTY(EditAnywhere)
		class UBoxComponent* MyRootComponent;

	// �������� ��� �����
	UPROPERTY(EditAnywhere)
		class UMaterialInstance* WallColor;

	//void CollideWall();

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
};
